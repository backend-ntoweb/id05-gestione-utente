INSERT INTO roles (name) VALUES ('ROLE_ADMIN');
INSERT INTO roles (name) VALUES ('ROLE_USER');
INSERT INTO roles (name) VALUES ('ROLE_PERSONA');

INSERT INTO users (username, password, email, enable) VALUES ('anderson','$2y$12$kMsA2rjhdrwf/BSr2ndVAuBjHmfQXMmNU7FQD/PW5tfznAeWDiR9e','email@gmail.com',1);
INSERT INTO users (username, password, email, enable) VALUES ('user','$2y$12$ne8TktKi.ahcgFaaZnSokeNaHHkDXybsQGtrWy7K.AkC0Jv1ZzPGG','user@gmail.com',1);

INSERT INTO users_roles (user_id,role_id) VALUES (1,1);
INSERT INTO users_roles (user_id,role_id) VALUES (1,2);
INSERT INTO users_roles (user_id,role_id) VALUES (2,2);
