package app.nike.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.nike.entity.User;
import app.nike.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/{usernameOrEmail}")
	public User getUserByUsernameOrEmail(@PathVariable(value = "usernameOrEmail") String userOrEmail) throws Exception {
		return userService.findByUsernameOrEmail(userOrEmail);
	}
	
	@PostMapping("/crea")
	public User createUser(@RequestBody User user) throws Exception {
		return userService.createUser(user);
	}
	
	@DeleteMapping("/delete/{username}")
	public void deleteUtente(@PathVariable(value ="username") String username) throws Exception {
		userService.deleteUserByUsername(username);
	}
	
	@PutMapping("/update")
	public User updateUser(@RequestBody User user) throws Exception {
		return userService.updateUser(user);
	}
}
