package app.nike.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import app.nike.entity.Role;
import app.nike.entity.User;
import app.nike.repository.RoleRepository;
import app.nike.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private BCryptPasswordEncoder passEncoder;

	@Override
	public User findByUsernameOrEmail(String usernameOrEmail) throws Exception {
		try {
			return userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail).get();
		} catch (Exception e) {
			throw new Exception("Username Or Email not Found --> Error : " + e.getMessage());
		}
	}

	@Override
	public User createUser(User user) throws Exception {
		try {
			if (userRepository.findByUsernameOrEmail(user.getUsername(), user.getEmail()).isPresent())
				throw new Exception("Username or Email is present");

			if (user.getRoles().isEmpty())
				throw new Exception("Nessun Ruolo Inserito");
			for (Role r : user.getRoles()) {
				Role roleDb = roleRepository.findRoleByName(r.getName());
				if (roleDb == null)
					throw new Exception("Il ruolo non esiste");
				user.getRoles().remove(r);
				user.getRoles().add(roleDb);
			}
			user.setPassword(passEncoder.encode(user.getPassword()));
			
			user.setEnable(true);
			
			return userRepository.save(user);

		} catch (Exception e) {
			throw new Exception("Lo user non è stato salvato --> Error: " + e.getMessage());
		}
	}

	@Override
	public void deleteUserByUsername(String username) throws Exception {
		try {
			User userToDelete = userRepository.findByUsername(username);

			if (userToDelete == null)
				throw new Exception("User " + username + " not found");

			userToDelete.getRoles().removeAll(userToDelete.getRoles());

			userRepository.delete(userToDelete);

		} catch (Exception e) {
			throw new Exception("La cancellazione non è andata a buon fine --> Error :" + e.getMessage());
		}
	}

	@Override
	public User updateUser(User user) throws Exception {

		try {
			User userToUpdate = userRepository.findByUsername(user.getUsername());

			if (userToUpdate == null)
				throw new Exception("User " + user.getUsername() + " not found");

			userToUpdate.setEmail(user.getEmail());

			userToUpdate.setPassword(passEncoder.encode(user.getPassword()));

			return userRepository.save(userToUpdate);

		} catch (Exception e) {
			throw new Exception("L'Update non è andato a buon fine --> Error : " + e.getMessage());
		}
	}
}
