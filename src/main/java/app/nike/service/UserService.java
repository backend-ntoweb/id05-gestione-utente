package app.nike.service;

import app.nike.entity.User;

public interface UserService {

	public User createUser(User user) throws Exception;

	public void deleteUserByUsername(String username) throws Exception;

	public User findByUsernameOrEmail(String username) throws Exception;

	public User updateUser(User user) throws Exception;
}
