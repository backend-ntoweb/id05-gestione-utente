package app.nike.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import app.nike.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
	
	public Role findRoleByName(String name);
}
