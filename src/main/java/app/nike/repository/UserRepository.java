package app.nike.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import app.nike.entity.User;

public interface UserRepository extends JpaRepository<User,Long>{
	
	public User findByUsername(String username);
	
	public void deleteByUsername(String username);
	
	public Optional<User> findByUsernameOrEmail(String username, String email);
}
