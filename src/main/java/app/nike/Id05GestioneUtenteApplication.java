package app.nike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class Id05GestioneUtenteApplication {

	public static void main(String[] args) {
		SpringApplication.run(Id05GestioneUtenteApplication.class, args);
	}

}
