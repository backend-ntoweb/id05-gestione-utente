# id05-gestione-utente

#Path
@GetMapping("/{usernameOrEmail}")
public User getUserByUsernameOrEmail(@PathVariable(value = "usernameOrEmail") String userOrEmail)

@PostMapping("/crea")
public User createUser(@RequestBody User user)

@DeleteMapping("/delete/{username}")
public void deleteUtente(@PathVariable(value ="username") String username)

@PutMapping("/update")
public User updateUser(@RequestBody User user)

#Description
Gestione Delle credenziali di accesso utile per gestire la autenticazione
vincoli su le entity di tipo username e email (unique per ogni user)
